// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
import { getFirestore, doc, getDoc, getDocs, collection } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
import { getDatabase,onValue,ref,set,get,child,update,remove } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import { getStorage, ref as refS,uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-auth.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration

const firebaseConfig = {
    apiKey: "AIzaSyChmoqm9Gx-4TKF1417RB-65ePgFkvbJCc",
    authDomain: "sitioweb-ef224.firebaseapp.com",
    databaseURL: "https://sitioweb-ef224-default-rtdb.firebaseio.com",
    projectId: "sitioweb-ef224",
    storageBucket: "sitioweb-ef224.appspot.com",
    messagingSenderId: "1014928153173",
    appId: "1:1014928153173:web:7fe3f0653130fae98712dc"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
 const database = getDatabase(app);
 const auth = getAuth();
 const db = getDatabase();

 var mostrardatos = document.getElementById('mostrarTodo');

 function mostrarDisponible(){
  
    const db = getDatabase();
    const dbRef = ref(db, 'productos');
    onValue(dbRef, (snapshot) => {
     mostrardatos.innerHTML=""
     snapshot.forEach((childSnapshot) => {
     const childKey = childSnapshot.key;
     const childData = childSnapshot.val();
    if (childData.estado == "habilitado"){
      mostrardatos.innerHTML = mostrardatos.innerHTML+
     "<div class='caja'>"+
     "<div>"+
     "<h1>" + childKey + "</h1>"+
     "<h1>"+ childData.nombre+"</h1>"+
     "<img src='"+childData.url+"' alt='"+childData.imgNombre+"'>"+
     "<p class='PF'>$$"+childData.precio+"</p>"+
     "<p class='PF'>"+childData.descripcion+"</p>"+
     "</div>"+
    "</div>"
    }else{
      
    }
     
   
     
     // ...
     });
    }, {
     onlyOnce: true
    });
    }

    var btnTodo = document.getElementById('todo');
    btnTodo.addEventListener('click', mostrarDisponible());

