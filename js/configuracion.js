
 // Import the functions you need from the SDKs you need
 import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
 import { getFirestore, doc, getDoc, getDocs, collection } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
 import { getDatabase,onValue,ref,set,get,child,update,remove } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import { getStorage, ref as refS,uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
 
// TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyBelX8wOUG3PlbHjDi99bQQlJNRw7EHg08",
    authDomain: "sitioweb-91c81.firebaseapp.com",
    projectId: "sitioweb-91c81",
    storageBucket: "sitioweb-91c81.appspot.com",
    messagingSenderId: "1004471577230",
    appId: "1:1004471577230:web:26c272babc54ff7b4fb7df"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
 const db = getDatabase();

 var btnInsertar = document.getElementById("btnInsertar");
 var btnBuscar = document.getElementById("btnBuscar");
 var btnActualizar = document.getElementById("btnActualizar");
 var btnBorrar = document.getElementById("btnBorrar");
 var btnTodos = document.getElementById("btnTodos");
 var lista = document.getElementById("lista");
 var btnLimpiar = document.getElementById('btnLimpiar');
 var archivo = document.getElementById('archivo');
 var verImagen = document.getElementById('verImagen');
// Insertar
var matricula = "";
 var nombre = "";
 var carrera = "";
 var genero = "";



 function leerInputs(){
 matricula = document.getElementById("matricula").value;
 nombre = document.getElementById("nombre").value;
 carrera = document.getElementById("carrera").value;
 genero = document.getElementById('genero').value;

 }




function insertDatos(){
leerInputs();
 var genero= document.getElementById("genero").value;
 set(ref(db,'alumnos/' + matricula),{
 nombre: nombre,
 carrera:carrera,
 genero:genero})
 .then((docRef) => {

  if(document.getElementById('matricula').value == 0 || document.getElementById('nombre').value == 0 || document.getElementById('carrera').value == 0){
      alert("Te falta seleccionar un campo")
  }else{
  alert("registro exitoso");
  mostrarAlumnos();
  console.log("datos" + matricula +" "+ nombre +" "+  carrera+" "+ genero)
  } 
  })
  .catch((error) => {
  alert("Error en el registro")
  });

 }








// mostrar datos
function mostrarAlumnos(){
  
const db = getDatabase();
const dbRef = ref(db, 'alumnos');
onValue(dbRef, (snapshot) => {
 lista.innerHTML=""
 snapshot.forEach((childSnapshot) => {
 const childKey = childSnapshot.key;
 const childData = childSnapshot.val();

 lista.innerHTML = "<div class='campo'> " + lista.innerHTML + childKey + "<br>" +
"Nombre: " +childData.nombre + "<br>" + "Carrera: " + childData.carrera + "<br>" + "Genero: " + childData.genero + "<br>" + "<br> </div>";
 console.log(childKey + ":");
 console.log(childData.nombre)
 // ...
 });
}, {
 onlyOnce: true
});
}



function actualizar(){
  if(document.getElementById('matricula').value == 0 || document.getElementById('nombre').value == 0 || document.getElementById('carrera').value == 0){
    alert("Te falta seleccionar un campo")
}else{
 leerInputs();
update(ref(db,'alumnos/'+ matricula),{
 nombre:nombre,
 carrera:carrera,
 genero :genero
}).then(()=>{
 alert("se realizo actualizacion");
 mostrarAlumnos();
})
.catch(()=>{
 alert("causo Erro " + error );
});
}
}




function escribirInpust(){
 document.getElementById('matricula').value= matricula
 document.getElementById('nombre').value= nombre;
 document.getElementById('carrera').value= carrera;
 document.getElementById('genero').value= genero;
}



function borrar(){
  if(document.getElementById('matricula').value == 0){
    alert("Te falta seleccionar un campo")
  }
  else{
 leerInputs();
 remove(ref(db,'alumnos/'+ matricula)).then(()=>{
 alert("se borro");
 mostrarAlumnos();
 })
 .catch(()=>{
 alert("causo Erro " + error );
 });



}
}
function mostrarDatos(){
  if(document.getElementById('matricula').value == 0){
    alert("Te falta seleccionar un campo")
  }
  else{
 leerInputs();
 console.log("mostrar datos ");
 const dbref = ref(db);
 get(child(dbref,'alumnos/'+ matricula)).then((snapshot)=>{
 if(snapshot.exists()){
 nombre = snapshot.val().nombre;
 carrera = snapshot.val().carrera;
 genero = snapshot.val().genero;
 console.log(genero);
 escribirInpust();
}
 else {

 alert("No existe");
 }
}).catch((error)=>{
 alert("error buscar" + error);
});
  }
}




function limpiar(){
 lista.innerHTML="";
 matricula="";
 nombre="";
 carrera="";
 genero=1;
 escribirInpust();
}




btnInsertar.addEventListener('click',insertDatos);
btnBuscar.addEventListener('click',mostrarDatos);
btnActualizar.addEventListener('click',actualizar);
btnBorrar.addEventListener('click',borrar);
btnTodos.addEventListener('click', mostrarAlumnos);
btnLimpiar.addEventListener('click', limpiar);



function cargarImagen(){

  const file = event.target.files[0];
  const name = event.target.files[0].name;

  const storage = getStorage();

  const storageRef = refS(storage, 'imagenes/' + name);


  uploadBytes(storageRef, file).then((snapshot) => {
    document.getElementById('imgNombre').value = name;
    alert('Se cargo el Archivo');
  })
}

function descargarImagen(){
  archivo = document.getElementById('imgNombre').value;
  // Create a reference to the file we want to download
const storage = getStorage();
const starsRef = refS(storage, 'imagenes/' + archivo);

// Get the download URL
getDownloadURL(starsRef)
  .then((url) => {
   document.getElementById('url').value=url;
   document.getElementById('imagen').scr=url;
   

  })
  .catch((error) => {
    // A full list of error codes is available at
    // https://firebase.google.com/docs/storage/web/handle-errors
    switch (error.code) {
      case 'storage/object-not-found':
        console.log("no existe")
        // File doesn't exist
        break;
      case 'storage/unauthorized':
        // User doesn't have permission to access the object
        console.log("no tiene permiso")
        break;
      case 'storage/canceled':
        console.log("no existe")
        // User canceled the upload
        break;

      // ...

      case 'storage/unknown':
        // Unknown error occurred, inspect the server response
        break;
    }
  });
  
}

archivo.addEventListener('change' ,cargarImagen);
verImagen.addEventListener('click', descargarImagen);